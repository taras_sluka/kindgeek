package students.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import students.aop.LogExecutionTime;
import students.model.Student;

@Service
@Slf4j
public class SortServiceImpl implements SortService {

    @Override
    @LogExecutionTime
    public List<Student> bubbleSort(List<Student> students) {
        int n = students.size();
        boolean swapNeeded;
        for (int i = 0; i < n - 1; i++) {
            swapNeeded = true;
            for (int j = 1; j < n - i; j++) {
                if (students.get(j - 1).compareTo(students.get(j)) > 0) {
                    Collections.swap(students, j, j - 1);
                    swapNeeded = false;
                }
            }
            if (swapNeeded) {
                break;
            }
        }
        return students;
    }

    @Override
    @LogExecutionTime
    public List<Student> heapSort(List<Student> students) {
        for (int i = students.size() / 2 - 1; i >= 0; i--) {
            heapify(students, students.size(), i);
        }
        for (int i = students.size() - 1; i >= 0; i--) {
            Collections.swap(students, 0, i);
            heapify(students, i, 0);
        }
        return students;
    }

    private void heapify(List<Student> students, int size, int rootElementIndex) {
        int leftIndex = 2 * rootElementIndex + 1;
        int rightIndex = 2 * rootElementIndex + 2;
        int largest = rootElementIndex;
        if (leftIndex < size && students.get(leftIndex).compareTo(students.get(largest)) > 0) {
            largest = leftIndex;
        }
        if (rightIndex < size && students.get(rightIndex).compareTo(students.get(largest)) > 0) {
            largest = rightIndex;
        }
        if (largest != rootElementIndex) {
            Collections.swap(students, rootElementIndex, largest);
            heapify(students, size, largest);
        }
    }

    @Override
    @LogExecutionTime
    public List<Student> mergeSort(List<Student> students) {
        int mid = students.size() / 2;
        if (mid == 0) {
            return students;
        }
        return merge(mergeSort(students.subList(0, mid)), mergeSort(students
                .subList(mid, students.size())), new ArrayList<>());
    }

    private List<Student> merge(List<Student> first, List<Student> second, List<Student> accumulator) {
        if (first.isEmpty()) {
            accumulator.addAll(second);
        } else if (second.isEmpty()) {
            accumulator.addAll(first);
        } else {
            if (first.get(0).compareTo(second.get(0)) < 0) {
                accumulator.add(first.get(0));
                return merge(first.subList(1, first.size()), second, accumulator);
            } else {
                accumulator.add(second.get(0));
                return merge(first, second.subList(1, second.size()), accumulator);
            }
        }
        return accumulator;
    }

    @Override
    @LogExecutionTime
    public List<Student> sort(List<Student> students, SortAlgorithm algorithm) {
        switch (algorithm) {
            case BUBBLE:
                return bubbleSort(students);
            case MERGE:
                return mergeSort(students);
            case HEAP:
                return heapSort(students);
            default:
                return bubbleSort(students);
        }
    }
}
