package students.services;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import students.exception.StudentException;
import students.exception.StudentParseException;
import students.model.Student;

public interface StudentIOService {

    ArrayList<String> readeFile(Path path) throws StudentParseException;

    void writeDataToFile(List<Student> students, File file) throws StudentException;
}
