package students.services;

public enum SortOrder {
    ASCENDING,
    DESCENDING
}
