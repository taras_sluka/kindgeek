package students.services;

public enum SortAlgorithm {
    BUBBLE,
    HEAP,
    MERGE,
}
