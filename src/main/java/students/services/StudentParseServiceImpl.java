package students.services;

import java.util.ArrayList;
import org.springframework.stereotype.Service;
import students.exception.StudentParseException;
import students.model.Student;

@Service

public class StudentParseServiceImpl implements StudentParseService {

    public static final String DELIMITER = ",";

    @Override
    public ArrayList<Student> parse(ArrayList<String> stringStudents) throws StudentParseException {
        ArrayList<Student> students = new ArrayList<>();
        if (stringStudents != null && !stringStudents.isEmpty()) {
            for (String stringStudent : stringStudents) {
                students.add(parse(stringStudent));
            }
        }
        return students;
    }

    @Override
    public Student parse(String stringStudent) throws StudentParseException {
        String[] strings = stringStudent.split(DELIMITER);
        if (strings.length == 2) {
            String id = strings[0];
            String performanceString = strings[1];
            Double performanceDouble = Double.valueOf(performanceString);
            return Student.builder().id(id).performance(performanceDouble).build();
        }
        throw new StudentParseException("student parse exception");
    }
}
