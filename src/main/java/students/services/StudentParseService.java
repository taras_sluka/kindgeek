package students.services;

import java.util.ArrayList;
import students.exception.StudentParseException;
import students.model.Student;

public interface StudentParseService {

    ArrayList<Student> parse(ArrayList<String> stringStudents) throws StudentParseException;

    Student parse(String stringStudent) throws StudentParseException;
}
