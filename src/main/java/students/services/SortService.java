package students.services;

import java.util.List;
import students.aop.LogExecutionTime;
import students.model.Student;

public interface SortService {

    List<Student> bubbleSort(List<Student> students);

    List<Student> heapSort(List<Student> students);

    List<Student> mergeSort(List<Student> students);

    List<Student> sort(List<Student> students, SortAlgorithm algorithm);
}
