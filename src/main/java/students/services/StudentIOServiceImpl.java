package students.services;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import students.exception.StudentException;
import students.exception.StudentParseException;
import students.model.Student;

@Service
@Slf4j
public class StudentIOServiceImpl implements StudentIOService {

    static private final String LINE_SEPARATOR = System.lineSeparator();

    @Override
    public ArrayList<String> readeFile(Path path) throws StudentParseException {
        ArrayList<String> lines = new ArrayList<>();
        try (Stream<String> stream = Files.lines(path)) {
            stream.forEach(lines::add);
        } catch (IOException e) {
            throw new StudentParseException(e.getMessage());
        }
        return lines;
    }

    @Override
    public void writeDataToFile(List<Student> students, File file) throws StudentException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            for (Student s : students) {
                writer.write(s.toString());
                writer.write(LINE_SEPARATOR);
            }
        } catch (Exception e) {
            throw new StudentException(e.getMessage());
        }


    }
}
