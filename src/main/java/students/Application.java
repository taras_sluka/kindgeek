package students;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ButtonGroup;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import students.exception.StudentParseException;
import students.model.Student;
import students.services.SortAlgorithm;
import students.services.SortService;
import students.services.StudentIOService;
import students.services.StudentParseService;

@SpringBootApplication
@Slf4j
public class Application extends JFrame implements ActionListener {

    private static final String   TITLE_APP                 = "Sort Students";
    private static final String   TITLE_RADIO_BUTTON_BUBBLE = "bubble";
    private static final String   TITLE_RADIO_BUTTON_HEAP   = "heap";
    private static final String   TITLE_RADIO_BUTTON_MERGE  = "merge";
    private static final String[] columnNames               = {"id", "performance"};

    private final SortService         sortService;
    private final StudentIOService    studentIOService;
    private final StudentParseService studentParseService;

    private JFileChooser  fileChooser;
    private JTable        table;
    private Button        openButton;
    private Button        saveButton;
    private Button        sortButton;
    private SortAlgorithm sortAlgorithm = SortAlgorithm.BUBBLE;
    private List<Student> students      = new ArrayList<>();


    @Autowired
    Application(SortService sortService, StudentIOService studentIOService, StudentParseService studentParseService) {
        initUI();
        this.sortService = sortService;
        this.studentIOService = studentIOService;
        this.studentParseService = studentParseService;
    }

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new SpringApplicationBuilder(Application.class).headless(false)
                .run(args);
        EventQueue.invokeLater(() -> {
            Application ex = context.getBean(Application.class);
            ex.setVisible(true);
        });

    }

    void initUI() {
        setLayout(new BorderLayout());
        setTitle(TITLE_APP);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(510, 510);
        table = new JTable();
        table.setPreferredScrollableViewportSize(new Dimension(500, 500));
        table.setFillsViewportHeight(true);
        JPanel buttonsPanel = createButtonsPanel();
        JPanel buttonGroup = createButtonGroup();
        add(buttonsPanel, BorderLayout.NORTH);
        add(buttonGroup, BorderLayout.SOUTH);
        JScrollPane scrollPane = new JScrollPane(table);
        this.add(scrollPane);
        fileChooser = createFileChooser();
    }

    private JPanel createButtonGroup() {
        ButtonGroup buttonGroup = new ButtonGroup();

        JRadioButton bubbleRadioButton = new JRadioButton(TITLE_RADIO_BUTTON_BUBBLE, true);
        bubbleRadioButton.addActionListener(e -> sortAlgorithm = SortAlgorithm.BUBBLE);

        JRadioButton heapRadioButton = new JRadioButton(TITLE_RADIO_BUTTON_HEAP, true);
        heapRadioButton.addActionListener(e -> sortAlgorithm = SortAlgorithm.HEAP);

        JRadioButton mergeRadioButton = new JRadioButton(TITLE_RADIO_BUTTON_MERGE, true);
        mergeRadioButton.addActionListener(e -> sortAlgorithm = SortAlgorithm.MERGE);

        buttonGroup.add(bubbleRadioButton);
        buttonGroup.add(heapRadioButton);
        buttonGroup.add(mergeRadioButton);
        JPanel jPanel = new JPanel();
        jPanel.add(bubbleRadioButton);
        jPanel.add(heapRadioButton);
        jPanel.add(mergeRadioButton);

        return jPanel;
    }

    private JPanel createButtonsPanel() {
        JPanel buttonPanel = new JPanel();
        openButton = createOpenFileButton();
        saveButton = createSaveFileButton();
        sortButton = createSortButton();
        buttonPanel.add(openButton);
        buttonPanel.add(saveButton);
        buttonPanel.add(sortButton);
        return buttonPanel;
    }

    private Button createOpenFileButton() {
        Button button = new Button("Open File");
        button.addActionListener(this);
        return button;
    }

    private Button createSaveFileButton() {
        Button button = new Button("Save to File");
        button.addActionListener(this);
        return button;
    }

    private Button createSortButton() {
        Button button = new Button("Sort");
        button.addActionListener(this);
        return button;
    }

    private JFileChooser createFileChooser() {
        JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir"));
        return fileChooser;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == openButton) {
            try {
                processOpenButtonAction();
            } catch (StudentParseException e1) {
                e1.printStackTrace();
            }
        } else if (e.getSource() == saveButton) {
            processSaveButtonAction();
        } else if (e.getSource() == sortButton) {
            processSortButtonAction();
        }

    }

    private void processOpenButtonAction() throws StudentParseException {
        int returnVal = fileChooser.showOpenDialog(Application.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            Path path = fileChooser.getSelectedFile().toPath();
            ArrayList<String> strings = studentIOService.readeFile(path);
            ArrayList<Student> students = studentParseService.parse(strings);
            updateTableData(students);
            this.students = students;
            log.debug("Opening: " + path);
        } else {
            log.debug("Open command cancelled by user");
        }
    }

    private void processSortButtonAction() {
        if (!students.isEmpty()) {
            SortAlgorithm sortAlgorithm = getSortAlgorithm();
            switch (sortAlgorithm) {
                case BUBBLE:
                    students = sortService.bubbleSort(students);
                    break;
                case HEAP:
                    students = sortService.heapSort(students);
                    break;
                case MERGE:
                    students = sortService.mergeSort(students);
                    break;
            }
            updateTableData(students);
        } else {
            showWarning("No data to sort");
        }
    }

    private void processSaveButtonAction() {
        int returnVal = fileChooser.showSaveDialog(Application.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            try {
                studentIOService.writeDataToFile(students, file);
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            log.debug("Saving: " + file.getName());
        } else {
            log.debug("Save command cancelled by user");
        }
    }

    private void updateTableData(List<Student> students) {
        DefaultTableModel tableModel = new DefaultTableModel(transformData(students), columnNames);
        table.setModel(tableModel);

    }

    private Object[][] transformData(List<Student> students) {
        Object[][] objects = new Object[students.size()][2];
        for (int i = 0; i < students.size(); i++) {
            Student student = students.get(i);
            String id = student.getId();
            Double performance = student.getPerformance();
            objects[i] = new Object[]{id, performance};
        }
        return objects;
    }

    private SortAlgorithm getSortAlgorithm() {
        return sortAlgorithm;
    }

    public void showWarning(String message) {
        JOptionPane.showMessageDialog(this, message, "Oops something wrong", JOptionPane.WARNING_MESSAGE);
    }

    public void showInfo(String message) {
        JOptionPane.showMessageDialog(this, message, "Congratulations", JOptionPane.INFORMATION_MESSAGE);
    }
}
