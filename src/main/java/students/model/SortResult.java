package students.model;

import lombok.Data;

@Data
public class SortResult {

    Long sortTime;
    Long count;

}
