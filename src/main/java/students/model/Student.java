package students.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Student implements Comparable<Student> {

    private String id;
    private Double performance;


    @Override
    public String toString() {
        return id + "," + performance;
    }

    @Override
    public int compareTo(Student o) {
        return this.performance.compareTo(o.performance);
    }
}
