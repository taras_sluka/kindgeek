package students.aop;


import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import students.Application;

@Aspect
@Component
@Slf4j
public class StudentAspect implements ApplicationContextAware {

    private ApplicationContext context;

    @Around("@annotation(students.aop.LogExecutionTime)")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.nanoTime();
        Object proceed = joinPoint.proceed();
        long executionTime = System.nanoTime() - start;

        if (proceed instanceof List) {
            int size = ((List) proceed).size();
            log.debug(joinPoint.getSignature() + " executed in " + executionTime + "ms");
            Application application = context.getBean(Application.class);
            String message = "method " + joinPoint.getSignature()
                    .getName() + " size " + size + " executed in " + executionTime + "ms";
            application.showInfo(message);
        }
        return proceed;
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
