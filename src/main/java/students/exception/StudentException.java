package students.exception;

public class StudentException extends Exception {

    public StudentException(String message) {
        super(message);
    }
}
