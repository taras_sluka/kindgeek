package students.exception;

public class StudentParseException extends StudentException{

    public StudentParseException(String s) {
        super(s);
    }
}
